# 24/04/2024

# variables and datatypes
# name = "muarif"
# age = 22
# cgpa = 9.42
# is_student = True
# print(type(name))

# strings in python
# name = "muarif shaikh"
# print(name[0])
# print(name[1:4])
# print(name[-1])
# print(name[0:6:2])

# reversing the string
# print(name[::-1])

# escape character in python
# \'
# \"
# \\
# \n
# name = "muarif\nshaikh"
# print(name)

# formating strings
# first_name = "muarif"
# last_name = "shaikh"
# full_name = first_name + " " + last_name   #concatenating strings
# print(full_name)
# full_name = f"{first_name} {last_name}"
# print(full_name)

# string methods
# name = "muarif shaikh"
# print(len(name))
# name = name.capitalize()
# print(name)
# print(name.upper())
# print(name.lower())
# print(name.title())
# print(name.find("sh"))
# print(name.find("qr"))  # will return -1
# print(name.replace("mu", ""))
# print("mu" in name)
# print("ka" not in name)
# name = "  muarif"
# print(name.strip())  # lstrip(), rstrip()


# numbers in python
# x = 1  # int
# y = 1.1  # float
# z = 3 + 2j  # complex number

# x += 3  # augmented assignment operator, x += 3 is same as x = x + 3
# print(x)

# print(round(2.6))
# print(round(2.5))
# print(abs(-1.7))  # will print 1.7

# import math
# print(math.ceil(2.2))
# print(math.floor(2.2))

# type conversion
# a = 2
# b = "3"
# b = int(b)
# print(a + b)

# falsy values
# ""
# 0
# None
# print(bool(""))
# print(bool(0))

# conditional statements
# age = int(input("Enter your age: "))
# if age < 1:
#     print("Invalid age")
# elif age >= 18:
#     print("you can vote")
# else:
#     print("you can't vote")

# ternary operator
# text = "you can vote" if age >= 18 else "you can't vote"
# print(text)

# logical operators
# and, or, not
# a = True
# b = True
# c = True
# if a and b and not c:
#     print("Hi")
# else:
#     print("by")

# chaining comparison operators
# age = 22
# if 18 <= age <= 99:
#     print("you can vote")

# for loop
# for i in range(1, 10, 2):
#     print("i =", i)

# for else loop 
# understanding it using the prime number question
# num = int(input("Enter a number: "))
# if num == 1:
#     print("1 is not a prime number")
# elif num > 1:
#     for i in range(2, num):
#         if num % i == 0:
#             print(num, "is not a prime number")
#             break
#     else:
#         print(num, "is a prime number")
# else:
#     print(num,"is not a prime number")

# nested loops
# for x in range(5):
#     for y in range(3):
#         print(f"({x}, {y})")

# while loop
# command = ""
# while command.lower() != "quit":
#     command = input(">")
#     print("Echo:", command)

# infinite loop
# while True:
#     command = input(">")
#     print("Echo:", command)
#     if command.lower() == "quit":
#         break

# functions in python
# def add(a, b):
#     return a + b
# add(3, 6)

# def printInfo(name, age=20):
#     return f"name: {name}, age: {age}"
# printInfo("muarif", 22)

# xargs
# def add(*numbers):
#     total = 1
#     for number in numbers:
#         total += number
#     return total
# print(add(2, 4, 7, 9))

# xxargs
# def save_info(**student):
#     print(student)
#     print(student["name"])
# save_info(id=1, name="muarif", age=22)

# scope of a variable
# age = 22 # global scope
# name = "muarif" # global scope
# def printAge():
#     age = 20 # local scope
#     return age, name
# print(printAge())
# print(age)

# fizz buzz question
# def fizz_buzz(input):
#     if (input % 3 == 0) and (input % 5 == 0):
#         return "fizzbuzz"
#     if input % 3 == 0:
#         return "fizz"
#     if input % 5 == 0:
#         return "buzz"
#     return input
# print(fizz_buzz(15))

# list in python
# mylist = ["muarif", 22, "mumbai", True]
# print(mylist)
# numlist = list(range(10))
# print(numlist)
# print(mylist + numlist)
# mylist[0] = "shaikh" # updating in item in the list
# print(mylist)

# list unpacking
# numbers = [1, 2, 3]
# first, second, third = numbers  # this will only work if the number of variables on the left side and the number of items in the list are equal. Otherwise it will throw error
# print(first)
# print(second)
# print(third)

# numbers = [1, 2, 3, 4, 5, 5, 6, 7, 8]
# first, second, *other = numbers
# print(first)
# print(second)
# print(other)

# looping over lists
# mylist = [1, "muarif", 22, "mumbai"]
# for index, item in enumerate(mylist):
#     print(index, item)

# adding and removing items in the list
# mylist = [1, "muarif", 22, True]
# add
# mylist.append("khed")
# mylist.insert(2, "shaikh")
# print(mylist)

# remove
# mylist.pop()
# mylist.pop(0)
# mylist.remove("shaikh")
# del mylist[0:2]
# mylist.clear()

# finding items in the list
# mylist = [1, "muarif", 22, True, 22]
# print(mylist.index("muarif"))
# print(mylist.count(22))

# sorting items in a list
# numlist = [23, 3, 45, 32]
# numlist.sort(reverse=True)
# print(numlist)
# print(sorted(numlist, reverse=True)) # sorting using sorted function

# mylist = [
#     ("joe", 12),
#     ("jos", 15),
#     ("jason", 10)
# ]
# def sort_item(item):
#     return item[1]
# mylist.sort(key=sort_item)
# print(mylist)

# lambda function
# mylist.sort(key=lambda item:item[1])
# print(mylist)

# map function
# x = map(lambda item: item[1], mylist)
# for item in x:
#     print(item)

# values = list(map(lambda item: item[1], mylist))
# print(values)

# filter function
# filtered = list(filter(lambda: item: item[1] >= 10, mylist))
# print(filtered)

# list compreshension
# values = [item[1] for item in mylist]  # mapping
# print(values)
# filtered = [item for item in mylist if item[1]> 10]  # filtering
# print(filtered)

# zip function
# l1 = [1, 2, 3]
# l2 = ["a", "b", 0]
# print(list(zip(l1, l2)))
# print(list(zip("xyz", l1, l2)))

# tuples
# tp = (1, 2, 3)
# tp2 = (4, 5, 6)
# tp3 = tp + tp2
# print(tp[1:3])

# swapping variables
# x = 10
# y = 15
# z = x
# x = y
# y = z

# x, y = y, x  # simple way to swap

# arrays
# from array import array
# numbers = array("i", [1, 2, 3, 5])

# sets
# numbers = [1, 2, 3, 3, 4, 5]
# set1 = set(numbers)
# print(set1)
# set2 = {2, 5}
# set2.add(6)
# print(set2)
# set2.remove(2)
# print(set2)

# print(set1 | set2) # union set
# print(set1 & set2) # intersection
# print(set1 - set2)
# print(set1 ^ set2)

# dictionaries
# dict1 = {"x":1, "y":2}
# dict1 = dict(x=1, y=2)
# print(dict1.get("x"))
# dict1["z"] = 3
# del dict1["x"]




# 25/04/2024

# Exception Handling
# try:
#     file = open("test.py")
#     age = int(input("Enter your age: "))
# except ValueError as ve:
#     print("invalid input")
#     print("Error:", ve)
#     print(type(ve))
# else:
#     print("no exceptions were thrown")
# finally:
#     file.close()  # finally clause is always executed whether we have an exeption or not.
# print("execution continues")

# except (ValueError, ZeroDivisionError)  <-- to pass multiple exceptions

# object oriented programming
# class Person:
#     def __init__(self): # constructor
#         self.name = "muarif"
#         self.age = 22

#     def printInfo(self):
#         print(f"Name: {self.name}, Age: {self.age}")

# p1 = Person()
# p2 = Person()
# p2.name = "shubham"
# p1.printInfo()
# p2.printInfo()

# class Student:
#     university = "MU" # class attribute
#     def __init__(self, rollno, name):
#         self.rollno = rollno
#         self.name = name

#     def printInfo(self):
#         print(f"Roll No: {self.rollno} Name: {self.name}")

# s1 = Student(1, "Amit")
# print(s1.university)
# print(Student.university)
# Student.university = "DU"

# class Student:
#     def __init__(self, rollno, name):
#         self.rollno = rollno
#         self.name = name

#     def printInfo(self):
#         print(f"Roll No: {self.rollno} Name: {self.name}")

#     def __str__(self):
#         return f"({self.rollno}, {self.name})"
    
#     def __eq__(self, other):
#         return self.rollno == other.rollno and self.name == other.name

#     @classmethod
#     def noneVal(cls):
#         return cls(None, None)

# s1 = Student(1, "Amit")
# print(s1)
# s2 = Student.noneVal()
# s1.printInfo()
# s2.printInfo()
# s3 = Student(1, "Amit")
# print(s1 == s3)

# Inheritance
# class A:
#     def __init__(self):
#         print("constructor of class A")

#     def feature1(self):
#         print("feature 1 working")

#     def feature2(self):
#         print("feature 2 working")

# class B(A):
#     def __init__(self):
#         print("constructor of clas B")

#     def feature3(self):
#         print("feature 3 working")
    
#     def feature4(self):
#         print("feature 4 working")

# class C(B):  # multilevel inheritance
#     def __init__(self):
#         print("constructor of class C")

#     def feature5(self):
#         print("feature 5 working")

#     def feature6(self):
#         print("feature 6 working")

# class D(C, B):  # multiple inheritance
#     def __init__(self):
#         print("constructor of clas D")

#     def feature7(self):
#         print("feature 7 working")
    
#     def feature8(self):
#         print("feature 8 working")

# c = C()
# c.feature1()

# method overriding
# class Person:
#     def __init__(self):
#         self.name = "muarif"
#         self.age = 22

# class Student(Person):
#     def __init__(self):
#         self.rollno = 1
#         super().__init__()

#     def printInfo(self):
#         print(self.rollno, self.name)

# s1 = Student()
# s1.printInfo()

# abstraction
# from abc import ABC, abstractmethod

# class Vehicle(ABC):
#     def __init__(self, name):
#         self.name = name
    
#     @abstractmethod
#     def move(self):
#         pass

# class Car(Vehicle):
    
#     def move(self):
#         return f"{self.name} is driving"

# class Plane(Vehicle):
    
#     def move(self):
#         return f"{self.name} is flying"
    
# car = Car("Toyota")
# plane = Plane("Air India")

# print(car.move())
# print(plane.move())

# encapsulation
# class Person:
#     def __init__(self, name):
#         self._name = name

#     def get_name(self):
#         return self._name
    
#     def set_name(self, name):
#         self._name = name

# person = Person("joe")
# print(person.get_name())
# person.set_name("jason")
# print(person.get_name())


# polymorphism
# class Animal:
#     def make_sound(self):
#         pass

# class Dog(Animal):
#     def make_sound(self):
#         return "Woof!"
    
# class Cat(Animal):
#     def make_sound(self):
#         return "Meow!"
    
# class Duck(Animal):
#     def make_sound(self):
#         return "Quack!"
    
# dog = Dog()
# cat = Cat()
# duck = Duck()

# animals = [dog, cat, duck]
# for animal in animals:
#     print(animal.make_sound())

# duck typing
# class Duck:
#     def quack(self):
#         print("Quack!")

# class Person:
#     def quack(self):
#         print("I'm quacking like a duck!")

# def make_quack(obj):
#     obj.quack()

# duck = Duck()
# person = Person()

# make_quack(duck)    # Output: Quack!
# make_quack(person)  # Output: I'm quacking like a duck!


# working wit path
# from pathlib import Path

# Creating a Path object
# path = Path("test_folder")

# Check if the path exists
# if path.exists():
#     print(f"The path '{path}' exists.")
# else:
#     print(f"The path '{path}' does not exist.")

# Create the directory if it doesn't exist
# if not path.exists():
#     path.mkdir()
#     print(f"Created the directory '{path}'.")

# List all files and directories in the path
# print("\nListing all files and directories:")
# for item in path.iterdir():
#     print(item)

# Create a new file inside the directory
# file_path = path / "example_file.txt"
# file_path.write_text("This is an example file created using pathlib.")
# print(f"\nCreated a new file '{file_path}'.")

# Read the contents of the file
# if file_path.exists():
#     file_contents = file_path.read_text()
#     print("\nContents of the file:")
#     print(file_contents)

# Deleting the file
# file_path.unlink()
# print(f"\nDeleted the file '{file_path}'.")

# Deleting the directory
# path.rmdir()
# print(f"\nDeleted the directory '{path}'.")

input_file = 'input.txt'
output_file = 'output.txt'

with open(input_file, 'r') as infile, open(output_file, 'w') as outfile:
    for line in infile:
        # Process the line (e.g., transform, filter, etc.)
        processed_line = line.upper()  # Convert to uppercase
        outfile.write(processed_line)
