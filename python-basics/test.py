# a=int(input("Enter the limit: "))
# for i in range(1,a+1):
#     if ((i%10) > 5) or ((i%10) == 0):
#         print("* ",  end="")
#     else:
#         print(i , " ", end="")


# asci_val = 65
# flag = True
# for i in range(5):
#     for j in range(i + 1):
#         alphabet = chr(asci_val)
#         if flag == True:
#             print(alphabet, end="")
#         else:
#             print(chr(asci_val+32), end="")
#         asci_val += 1


# name = "shashikant"
# n_list = []
# for char in name:
#     if char not in n_list:
#         n_list.append(char)
# result = "".join(n_list)
# print(result)


# name = "alexander"
# char_dict = {}
# for char in name:
#     char_dict[char]=char_dict.get(char, 0) + 1
# print(char_dict)


# def print_star_diamond(rows):
#     for i in range(1, rows + 1):
#         print(" " * (rows - i) + "* " * i)
#     for i in range(rows - 1, 0, -1):
#         print(" " * (rows - i) + "* " * i)
# print_star_diamond(5)


def print_hollow_square(n):
    for i in range(n):
        for j in range(n):
            if i == 0 or i == n - 1 or j == 0 or j == n - 1:
                # Print '*' for the border of the square
                print('*', end=' ')
            else:
                # Print empty space for the interior of the square
                print(' ', end=' ')
        print()  # Move to the next line after each row

# Get the input size of the square from the user
size = int(input("Enter the size of the square: "))

# Call the function to print the hollow square pattern
print_hollow_square(size)


# pascal triangle
def print_pascals_triangle(rows):
    for i in range(rows):
        coef = 1
        for j in range(1, rows - i):
            print(" ", end="")
        for k in range(0, i + 1):
            print(" ", coef, end="")
            coef = coef * (i - k) // (k + 1)
        print()

print_pascals_triangle(5)  # Print Pascal's Triangle with 5 rows


# fibonacci sequence
def fibonacci_sequence(n):
    fib_sequence = []
    a, b = 0, 1
    for _ in range(n):
        fib_sequence.append(a)
        a, b = b, a + b
    return fib_sequence

n_terms = 10  # Number of terms in the Fibonacci sequence
fib_result = fibonacci_sequence(n_terms)
print("Fibonacci Sequence:", fib_result)
