# pagination

>>> from orm.models import *
>>> from django.core.paginator import Paginator 
>>> objects = ["alex", "albert", "smith", "jason", "nathon"]    
>>> p = Paginator(objects, 2)
>>> p.count
5   
>>> p.num_pages
3
>>> page1 = p.page(1)
>>> page1
<Page 1 of 3>
>>> p.page_range
range(1, 4)
>>> page1.object_list
['alex', 'albert']
>>> page2 = p.page(2)
>>> page2.object_list
['smith', 'jason']
>>> page1.has_next()
True
>>> page1.has_previous()
False
>>> page1.has_other_pages()
True
>>> page1.next_page_number()
2
>>> page2.start_index()
3
>>> page2.end_index()
4




>>> Person.objects.bulk_create([Person(name="alex",age=22),Person(name="albert",age=20),Person(name="jason",age=33)])
[<Person: Person object (3)>, <Person: Person object (4)>, <Person: Person object (5)>]

>>> objects_to_update = Person.objects.filter(name__startswith="a")
>>> for obj in objects_to_update:
...     obj.age = 25

>>> Person.objects.bulk_update(objects_to_update, ['age']) 
3




# reverting migrations
# to revert back to migration 0003 from 0004
python manage.py migrate home 0003 # this will unapply migration 0004
# next step is to delete the migration 0004 and undo the changes in the code.

# reverting migrations using git
git log --online # displays list of commits
git reset --hard HEAD~1 # moving the head pointer one step back so that it points the previous commit


