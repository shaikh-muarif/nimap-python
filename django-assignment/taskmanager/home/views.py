from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db import transaction, connection
from .models import *
from .forms import *

def home(request):
    # tasks_with_subtasks = Task.objects.prefetch_related('subtask_set')
    # for task in tasks_with_subtasks:
    #     print(f"Task: {task.task_name}")
    # for subtask in task.subtask_set.all():
    #     print(f"  Subtask: {subtask.subtask_name}")
    return render (request, 'index.html')


def login_page(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password') 
        if not User.objects.filter(username = username).exists():
            messages.error(request, 'User Not Found!')
            return redirect('/task-manager/login/')
        user = authenticate(username = username, password = password)
        if user is None:
            messages.error(request, 'Invalid Password')
            return redirect('/task-manager/login/')
        else:
            login(request, user)
            return redirect('/task-manager/')
    return render(request, 'login.html')


def logout_page(request):
    logout(request)
    return redirect('/task-manager/login/')


@login_required(login_url="/task-manager/login/")
def task_list(request, val):
    if val == 0:
        task_list = Task.objects.filter(owner=request.user).filter(status=0)
    if val == 1:
        task_list = Task.objects.filter(owner=request.user).filter(status=1)
    if val == 2:
        task_list = Task.objects.filter(owner=request.user)
    paginator = Paginator(task_list, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'task_list.html', {'page_obj': page_obj})


@login_required(login_url="/task-manager/login/")
def create_task(request):
    if request.method == 'POST':
        data = request.POST
        task_name = data.get('taskname')
        Task.objects.create(task_name = task_name, owner = request.user)
        messages.info(request, 'Task created successfully!')
        return redirect('/task-manager/create-task/')
    return render(request, 'create_task.html')


@login_required(login_url="/task-manager/login/")
def task_detail(request, id):
    task = Task.objects.get(id = id)
    subtasks = task.subtask_set.all()
    status_display = task.get_status_display()
    if request.method == 'POST':
        data = request.POST
        subtask_name = data.get('subtaskname')
        Subtask.objects.create(subtask_name = subtask_name, task = task)
    return render(request, 'task_detail.html', context={"task":task, "subtasks":subtasks, "status_display":status_display})


@login_required(login_url="/task-manager/login/")
def delete_task(request, id):
    task = Task.objects.get(id = id)
    if task.owner == request.user:
        task.delete()
        return redirect('/task-manager/task-list/2/')
    else:
        return render(request, 'permission_denied.html') 


@login_required(login_url="/task-manager/login/")
def edit_task(request, id):
    task = Task.objects.get(id=id)
    status_display = task.get_status_display() # used for fetching the value of status_choice.
    if request.method == 'POST':
        data = request.POST
        taskname = data.get('taskname')
        status = data.get('status')  
        task.task_name = taskname
        task.status = status
        task.owner = request.user
        task.save()
    return render(request, 'edit_task.html', context={"task":task, "status_display":status_display})


@login_required(login_url="/task-manager/login/")
def delete_subtask(request, id):
    subtask = Subtask.objects.select_related("task").get(id = id)
    task_id = subtask.task.id
    if subtask.task.owner == request.user:
        subtask.delete()
        return redirect('task_detail', id=task_id)
    else:
        return render(request, 'permission_denied.html')




# class TaskDetailView(View):
#     @login_required(login_url="/task-manager/login/")
#     def get(self, request, id):
#         task = Task.objects.get(id=id)
#         subtasks = task.subtask_set.all()
#         status_display = task.get_status_display()
#         return render(request, 'task_detail.html', context={"task": task, "subtasks": subtasks, "status_display": status_display})
    
#     @login_required(login_url="/task-manager/login/")
#     def post(self, request, id):
#         task = Task.objects.get(id=id)
#         if request.method == 'POST':
#             subtask_name = request.POST.get('subtaskname')
#             Subtask.objects.create(subtask_name=subtask_name, task=task)
#         return redirect('task_detail', id=id)


# class DeleteSubtaskView(View):
#     @login_required(login_url="/task-manager/login/")
#     def get(self, request, id):
#         subtask = Subtask.objects.select_related("task").get(id=id)
#         task_id = subtask.task.id
#         if subtask.task.owner == request.user:
#             subtask.delete()
#             return redirect('task_detail', id=task_id)
#         else:
#             return render(request, 'permission_denied.html')
        


# def view_form(request):
#     if request.method == "POST":
#         data = request.POST
#         name = data.get("name")
#         age = data.get("age")
#         Person.objects.create(name=name, age=age)
#     return render(request, "forms_example.html", context={"form":PersonForm()})




# def add_task(request):
#     with transaction.atomic():
#         owner = request.user
#         task = Task(task_name="xyz task", status=0, owner=owner)
#         task.save()

#         subtask = Subtask(subtask_name="subtask 1", task=task)
#         subtask.save()

#     return render(request, "index.html")



# def raw_sql(request):
#     # queryset = Task.objects.raw("INSERT INTO home_task VALUES('xyz task inserted using raw SQL', 0, 2 )")
#     insert_data = "INSERT INTO home_task VALUES('xyz task inserted using raw SQL', 0, 2 )"
#     with connection.cursor() as cursor:
#         cursor.execute(insert_data)
#     return render(request, "index.html")
