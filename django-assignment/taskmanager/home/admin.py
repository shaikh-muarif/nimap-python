from django.contrib import admin
from .models import Task, Subtask, RequestLog, Person

# Register your models here.

admin.site.register(Task)
admin.site.register(Subtask)
admin.site.register(RequestLog)
admin.site.register(Person)