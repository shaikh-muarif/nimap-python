from django.db import models
from django.contrib.auth.models import User

class Task(models.Model):
    STATUS_CHOICES = (
        (0, 'Open'),
        (1, 'Closed'),
    )

    task_name = models.CharField(max_length=100)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.task_name
    
class Subtask(models.Model):
    subtask_name = models.CharField(max_length=100)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subtask_name
    

class RequestLog(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Request at {self.timestamp}"


# model for form
class Person(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField(default=0)