from .models import RequestLog
from django.utils import timezone

# class RequestLogMiddleware:
#     def __init__(self, get_response):
#         self.get_response = get_response
#         self.log_file = 'request_logs.txt'  # Name of the log file

#     def __call__(self, request):
#         # Log the request details to a file
#         with open(self.log_file, 'a') as f:
#             log_message = f"Request URL: {request.path}, Method: {request.method}\n"
#             f.write(log_message)

#         # Pass the request to the next middleware or view
#         response = self.get_response(request)

#         # Return the response
#         return response
    


class RequestLoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        response = self.get_response(request)

        if request.path == "/task-manager/":
            RequestLog.objects.create(timestamp=timezone.now())

        return response