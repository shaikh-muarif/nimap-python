from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home_page"),
    path('login/', views.login_page, name="login_page"),
    path('logout/', views.logout_page, name="logout_page"),
    path('task-list/<int:val>/', views.task_list, name="task_list"),
    path('task-detail/<int:id>/', views.task_detail, name="task_detail"),
    path('create-task/', views.create_task, name="create_task"),
    path('edit-task/<int:id>/', views.edit_task, name="edit_task"),
    path('delete-task/<int:id>/', views.delete_task, name="delete_task"),
    path('delete-subtask/<int:id>/', views.delete_subtask, name="delete_subtask"),
    # path('task-detail/<int:id>/', views.TaskDetailView.as_view(), name='task_detail'),
    # path('delete-subtask/<int:id>/', views.DeleteSubtaskView.as_view(), name='delete_subtask'),

    #path('view-form/', views.view_form, name="view_form"),
    #path('add-task/', views.add_task), 
]

