from django.db import models


class UserProfile(models.Model):
    GENDER_MALE = 'M'
    GENDER_FEMALE = 'F'
    GENDER_CHOICES = [
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, 'Female')
    ]
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    phone = models.IntegerField(default=0)
    birth_date = models.DateField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=GENDER_MALE)
    

class User(models.Model):
    username = models.CharField(max_length=15)
    user_info = models.OneToOneField(UserProfile, null=True, on_delete=models.SET_NULL)


class Category(models.Model):
    category_name = models.CharField(max_length=100, unique=True)
    description = models.TextField()

    
class Tag(models.Model):
    tag_name = models.CharField(max_length=100)

class Product(models.Model):
    product_name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    product_image = models.ImageField(upload_to='product-images/', null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product_name
    
