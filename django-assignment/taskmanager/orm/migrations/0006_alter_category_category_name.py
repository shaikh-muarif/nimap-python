# Generated by Django 5.0.6 on 2024-05-11 09:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orm', '0005_rename_user_user_user_info_user_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='category_name',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
