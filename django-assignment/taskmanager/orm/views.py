from django.shortcuts import render, redirect
from django.views import View
from .models import UserProfile, Product

class UserProfileListView(View):
    def get(self, request):
        userprofiles = UserProfile.objects.select_related('user').all()
        return render(request, 'userprofile_list.html', {'userprofiles': userprofiles})

class UserProfileDetailView(View):
    def get(self, request, pk):
        userprofile = UserProfile.objects.select_related('user').get(pk=pk)
        return render(request, 'userprofile_detail.html', {'userprofile': userprofile})

class UserProfileCreateView(View):
    def get(self, request):
        return render(request, 'userprofile_form.html')

    def post(self, request):
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        birth_date = request.POST.get('birth_date')
        gender = request.POST.get('gender')
        
        userprofile = UserProfile.objects.create(
            name=name,
            email=email,
            phone=phone,
            birth_date=birth_date,
            gender=gender
        )
        return redirect('userprofile-detail', pk=userprofile.pk)

class ProductListView(View):
    def get(self, request):
        products = Product.objects.select_related('category').prefetch_related('tags').all()
        return render(request, 'product_list.html', {'products': products})

class ProductDetailView(View):
    def get(self, request, pk):
        product = Product.objects.select_related('category').prefetch_related('tags').get(pk=pk)
        return render(request, 'product_detail.html', {'product': product})

class ProductCreateView(View):
    def get(self, request):
        return render(request, 'product_form.html')

    def post(self, request):
        product_name = request.POST.get('product_name')
        price = request.POST.get('price')
        description = request.POST.get('description')
        category_id = request.POST.get('category')
        product_image = request.FILES.get('product_image')
        tag_ids = request.POST.getlist('tags')
        
        product = Product.objects.create(
            product_name=product_name,
            price=price,
            description=description,
            category_id=category_id,
            product_image=product_image
        )
        product.tags.add(*tag_ids)
        return redirect('product-detail', pk=product.pk)
