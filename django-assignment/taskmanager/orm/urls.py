from django.urls import path
from .views import (
    UserProfileListView, UserProfileDetailView, UserProfileCreateView,
    ProductListView, ProductDetailView, ProductCreateView
)

urlpatterns = [
    path('userprofiles/', UserProfileListView.as_view(), name='userprofile-list'),
    path('userprofiles/<int:pk>/', UserProfileDetailView.as_view(), name='userprofile-detail'),
    path('userprofiles/create/', UserProfileCreateView.as_view(), name='userprofile-create'),

    path('products/', ProductListView.as_view(), name='product-list'),
    path('products/<int:pk>/', ProductDetailView.as_view(), name='product-detail'),
    path('products/create/', ProductCreateView.as_view(), name='product-create'),
]
