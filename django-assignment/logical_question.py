
num_dict = {
    "I":1,
    "V":5,
    "X":10,
    "L":50,
    "C":100,
    "D":500,
    "M":1000
}

num = input("Enter a roman number: ")
x = 0
for i in num:
    if num.index(i) < len(num)-1:
        next_num = num[num.index(i)+1]
        if num_dict.get(i) < num_dict.get(next_num):
            num_dict[i] = -(num_dict.get(i))
        else:
            num_dict[i] = num_dict.get(i)
    if i in num_dict.keys():
        y = num_dict.get(i)
    x += y

print(x)


