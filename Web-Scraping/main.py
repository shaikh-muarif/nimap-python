import requests
from bs4 import BeautifulSoup

def summary_scrape(symbol):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
    }
    url = f'https://finance.yahoo.com/quote/{symbol}/'
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    previous_close = soup.find("fin-streamer", {"data-field":"regularMarketPreviousClose"}).text
    market_cap = soup.find("fin-streamer", {"data-field":"marketCap"}).text
    pe_ratio = soup.find("fin-streamer", {"data-field":"trailingPE"}).text

    return {
        "previous_close": previous_close,
        "market_cap": market_cap,
        "pe_ratio": pe_ratio
        }

print(summary_scrape("DIN"))
    
    
