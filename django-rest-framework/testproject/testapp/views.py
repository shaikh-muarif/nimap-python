from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
#from rest_framework.pagination import PageNumberPagination
from .filters import BookFilter
from .models import Book, Author, Genre
from .serializers import BookSerializer, AuthorSerializer, GenreSerializer, UserSerializer
from .pagination import DefaultPagination
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken

class RegisterUser(APIView):
    def post(self, request):
        serializer = UserSerializer(data = request.data)

        if not serializer.is_valid():
            return Response({'Errors':serializer.errors})
        serializer.save()
        user = User.objects.get(username=serializer.data['username'])
        # token , _ = Token.objects.get_or_create(user=user)
        refresh = RefreshToken.for_user(user)

        return Response({'status': 200, 'payload': serializer.data, 
                         #'token':str(token)
                         'refresh': str(refresh),
                         'access': str(refresh.access_token),
                         })




#generic filtering
class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_class = BookFilter
    #pagination_class = PageNumberPagination
    pagination_class = DefaultPagination
    search_fields = ['title']
    ordering_fields = ['title', 'author']

    # authentication_classes = [TokenAuthentication]
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]


# filtering
# class BookViewSet(ModelViewSet):
#     serializer_class = BookSerializer

#     def get_queryset(self):
#         queryset = Book.objects.all()
#         author_id = self.request.query_params['author']
#         if author_id is not None:
#             queryset = queryset.filter(author_id=author_id)

#         return queryset

class GenreViewSet(ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer

class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


# class AuthorList(generics.ListCreateAPIView):
#     queryset = Author.objects.all()
#     serializer_class = AuthorSerializer
 

# class AuthorDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Author.objects.all()
#     serializer_class = AuthorSerializer



# class AuthorList(APIView):
#     def get(self, request):
#         authors = Author.objects.all()
#         serializer = AuthorSerializer(authors, many=True)
#         return Response(serializer.data)
#     def post(self, request):
#         serializer = AuthorSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data, status=status.HTTP_201_CREATED)



# class AuthorDetail(APIView):
#     def get(self, request, id):
#         author = get_object_or_404(Author, pk=id)
#         serializer = AuthorSerializer(author)
#         return Response(serializer.data)
#     def put(self, request, id):
#         author = get_object_or_404(Author, pk=id)
#         serializer = AuthorSerializer(author, data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)
#     def delete(self, request, id):
#         author = get_object_or_404(Author, pk=id)
#         author.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)




# @api_view(['GET', 'POST'])
# def author_list(request):
#     authors = Author.objects.all()
#     if request.method == 'GET':
#         serializer = AuthorSerializer(authors, many=True)
#         return Response(serializer.data)
#     elif request.method == 'POST':
#         serializer = AuthorSerializer(data=request.data)
#         if serializer.is_valid(raise_exception=True):
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)


# @api_view(['GET', 'PUT', 'DELETE'])
# def author_detail(request, id):
#     author = get_object_or_404(Author, pk=id)
#     if request.method == 'GET':
#         serializer = AuthorSerializer(author)
#         return Response(serializer.data)
#     elif request.method == 'PUT':
#         serializer = AuthorSerializer(author, data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)
#     elif request.method == 'DELETE':
#         author.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
    
    




@api_view(['GET', 'POST'])
def books(request):
    if request.method == 'GET':
        books = Book.objects.select_related('author').all()
        serializer = BookSerializer(books, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)
        
        
        # if serializer.is_valid():
        #     serializer.validated_data
        #     return Response(serializer.data)
        # else:
        #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

# @api_view(['GET'])
# def book_info(request, id):
#     try:
#         book = Book.objects.get(id=id)
#         serializer = BookSerializer(book)
#         return Response(serializer.data)
#     except Book.DoesNotExist:
#         # return Response(status=404)
#         return Response(status=status.HTTP_404_NOT_FOUND)
    
@api_view(['GET'])
def book_info(request, id):
    book = get_object_or_404(Book, pk=id)
    serializer = BookSerializer(book)
    return Response(serializer.data)
