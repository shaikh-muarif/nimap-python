from django.urls import path, include
from rest_framework.routers import SimpleRouter, DefaultRouter
from rest_framework_nested.routers import NestedSimpleRouter
from . import views
from .views import GenreViewSet, AuthorViewSet, BookViewSet

router = DefaultRouter()

router.register('genres', GenreViewSet)
router.register('authors', AuthorViewSet)
router.register('books', BookViewSet, basename="books")

#urlpatterns = router.urls

# urlpatterns = [
#     path('', include(router.urls)),
#     # path('books/', views.books, name="books"),
#     # path('books/<int:id>/', views.book_info, name="book_info"),
#     # path('author/', views.author_list, name="authors"),
#     # path('author/<int:id>/', views.author_detail, name="author-info"),
#     # path('author/', views.AuthorList.as_view(), name="authors"),
#     # path('author/<int:pk>/', views.AuthorDetail.as_view(), name="author-info"),
# ]


# Nested routers for books under authors
authors_router = NestedSimpleRouter(router, 'authors', lookup='author')
authors_router.register('books', BookViewSet, basename='books')

urlpatterns = [
    path('', include(router.urls)),
    path('', include(authors_router.urls)),
    path('register/', views.RegisterUser.as_view())
]
