from django_filters.rest_framework import FilterSet
from .models import Book

class BookFilter(FilterSet):
    class Meta:
        model = Book
        fields = {
            'author_id': ['exact'],
            'publication_date': ['gt', 'lt']
        }