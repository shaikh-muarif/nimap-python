from django.contrib import admin
from .models import *

# Register your models here.
# admin.site.register(User)
# admin.site.register(UserProfile)
# admin.site.register(Student)
# admin.site.register(Department)

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ['name', 'age', 'eligibility']
    list_editable = ['age']
    list_per_page = 5

    @admin.display(ordering='age')
    def eligibility(self, person):
        if person.age > 18:
            return "yes"
        return "no"
    