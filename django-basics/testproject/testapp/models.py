from django.db import models

# one to one relationship
# class User(models.Model):
#     username = models.CharField(max_length=100, unique=True)
#     email = models.EmailField(unique=True)

# class UserProfile(models.Model):
#     user = models.OneToOneField(User, null=True, on_delete=models.SET_NULL)

# one to many relationship
class Author(models.Model):
    name = models.CharField(max_length=100)

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

# many to many relationship
# class Tag(models.Model):
#     name = models.CharField(max_length=100)

# class Post(models.Model):
#     title = models.CharField(max_length=100)
#     content = models.TextField()
#     tags = models.ManyToManyField(Tag)

class Category(models.Model):
    name = models.CharField(max_length=255)
    
class Item(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=6)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name



# class Department(models.Model):
#     dept_name = models.CharField(max_length=255)

# class Student(models.Model):
#     GENDER_MALE = 'M'
#     GENDER_FEMALE = 'F'
#     GENDER_CHOICES = [
#         (GENDER_MALE, 'Male'),
#         (GENDER_FEMALE, 'Female')
#     ]  # choice fields
#     name = models.CharField(max_length=255)
#     birth_date = models.DateField(null=True)
#     email = models.EmailField(unique=True)
#     phone = models.IntegerField(max_length=10, default=0)
#     gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=GENDER_MALE)
#     department = models.ForeignKey(Department, on_delete=models.CASCADE)



class Person(models.Model):
    name = models.CharField(max_length=255)
    age = models.IntegerField()
    city = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    # class Meta:
    #     db_table = 'people'
    #     indexes = [
    #         models.Index(fields = ['name', 'age'])
    #     ]
