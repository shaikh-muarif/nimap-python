from django.shortcuts import render
from django.db.models import Q, F
from django.core.paginator import Paginator
from .models import *

# Create your views here.

# def home(request):

#     queryset = Person.objects.filter(pk=1)
#     for person in queryset:
#         p_dict = {'name':person.name, 'age':person.age, 'city':person.city}

#     return render(request, 'index.html', context={'person':p_dict})


def item_list(request):
    item_list = Item.objects.all()
    paginator = Paginator(item_list, 10)  # Show 10 items per page

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    return render(request, 'test.html', {'page_obj': page_obj})



def home(request):

    queryset = Book.objects.select_related('author').all()
    person = Person.objects.raw("select * from testapp_person")

    return render(request, 'index.html', context={'books':queryset, 'person':list(person)})
